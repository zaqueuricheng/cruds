$CLUSTER_NAME="democlustercli"
$CLUSTER_ZONE="us-central1-a"

# Stop cluster
gcloud container clusters resize $CLUSTER_NAME --size=0 --zone $CLUSTER_ZONE
gcloud container clusters resize $CLUSTER_NAME --num-nodes=0 --zone $CLUSTER_ZONE

# Start cluster
gcloud container clusters resize $CLUSTER_NAME --size=1 --zone $CLUSTER_ZONE
gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

# Update
gcloud container clusters update $CLUSTER_NAME --zone $CLUSTER_ZONE --enable-autoscaling --min-nodes 0 --max-nodes 10

# Tinha redimencionado o cluster e cancelado no caminho, ai ficou num 
# loop infinito por sinal usando o comando abaixo resolveu (Apagar o node é como se tivesse redimencionado para 
# zero nodes, este comando força o redimencionamento)
kubectl delete node gke-democlustercli-default-pool-92b0c6dc-jgxt

kubectl describe node YOUR-NODE

kubectl top nodes
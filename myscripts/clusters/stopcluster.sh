# variables
CLUSTER_NAME="democlustercli"
CLUSTER_ZONE="us-central1-a"
CLUSTER_REGION=""

# test if variables works
echo $CLUSTER_NAME >> /home/node-01/stop_works.txt
echo $CLUSTER_ZONE >> /home/node-01/stop_works.txt

# command to resize cluster for stop all pods and services
yes Y | gcloud container clusters resize $CLUSTER_NAME --size=0 --zone $CLUSTER_ZONE
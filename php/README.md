# Crud com PHP
# Tecnologias
Figma, Gitlab, HTML, CSS, JS, PHP e MySQL (Docker)
# GitLab
    - User: zaqueuricheng
    - Pwd: gitlab
# Figma
    - link: https://www.figma.com/file/TUyILtdDEJOXGg9HezJ6pi/Untitled?node-id=1%3A8
    - User: zaqueu92804@hotmail.com
    - Pwd: figma

# Icons
    - https://icons8.com.br/icons/set/editar
    - https://www.flaticon.com/search?word=user

# Install XAMP on Windows
https://www.youtube.com/watch?v=EvCajb4Ujeg

    1 - https://www.apachefriends.org/pt_br
    2 - execute the file that you download
    3 - select the program that you need

# Configure VSCode for PHP
    https://www.youtube.com/watch?v=DFPvnxIgwKA

# MySQL docker to connect our API on data-base (Localhost)
    - docker pull mysql
    - docker run --name root -e MYSQL_ROOT_PASSWORD=92Z@queu -d mysql
    - docker container ls
    - docker container inspect ID
    - docker exec -it ID bash
        -  mysql -u root -p[Enter]
           //enter your localhost password
    - docker logs ID

# MySQL XAMP connect (Localhost)
    - Instalar o XAMP (Apache e MySQL)
    - Atualizar as variaveis /class/conexao
        - servidor: "localhost"
        - usuario: "root"
        - senha: ""
        - dbname: "usuarios"

# MySQL Cloud connect (Google Cloud)
    - DBeaver - Gerenciador de banco de dados
    - Banco de dados relacionais e não relacionais
    - Workbanch
    - PHPMyADM

# Install LAMP on Ubuntu
    - https://devanswers.co/install-apache-mysql-php-lamp-stack-ubuntu-20-04/
<?php 
	include("../backend/conexao.php"); /* Volta para a raiz depois entra na pasta backend/conexao.php*/

	// Listar os usuários do banco de dados (*)-> para pegar todas as colunas
	$consulta = "select * from users"; // users é o nome da tabela

	// Verifica a consulta
	$con = $mysqli->query($consulta) or die($mysqli->error);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>usuarios</title>
</head>
<body>
	<h1>
		<p1>Lista de usuários</p1>
	</h1>

	<!-- Criar tabela para mostrar os dados do bd -->
	<!-- tabela com o mesmo nr de colunas da tabela -->
	<table border="1">
		<!-- Nome das colunas -->
		<tr>
			<td>Codigo</td>
			<td>Nome</td>
			<td>Sobrenome</td>
			<td>Sexo</td>
			<td>E-mail</td>
			<td>Senha</td>
			<td>Acesso</td>
			<td>Data de Cadastro</td>
		</tr>

		<!-- PHP -->
		<?php
				while ($dado = $con->fetch_array()) {?>
					<tr>
						<td> <?php echo $dado["codigo"]?> </td>
						<td> <?php echo $dado["nome"]?> </td>
						<td> <?php echo $dado["sobrenome"]?> </td>
						<td> <?php echo $dado["sexo"]?> </td>
						<td> <?php echo $dado["email"]?> </td>
						<td> <?php echo $dado["senha"]?> </td>
						<td> <?php echo $dado["niveldeacesso"]?> </td>
						<td> <?php echo $dado["datadecadastro"]?> </td>
					</tr>
					<?php
				}
		?>
		<!-- PHP -->
	</table>

	<a href="../index.php"> <h2>Menu</h2><a/> <!-- ../ caminho relativo, volta um diretorio -->
</body>
</html>
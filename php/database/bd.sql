/* Para usar o My SQL via terminal - Docker container */
mysql -u root -p /* mysql -u <USER> -p <PWD>  

/* Sistema de login PHP + MySQL */

show databases;

/* */
create database usuarios; /* drop database usuarios; */

/* */
show databases;

/* */
use usuarios;

/* */
create table users(
	codigo int not null primary key auto_increment,
    nome varchar(50) not null unique,
    sobrenome varchar(50),
    sexo tinyint null,
    email varchar(50),
    senha varchar(30),
    niveldeacesso tinyint,
    datadecadastro datetime default current_timestamp
);

/* */
drop table users;

/* */
insert into users values(
	'id'
	'nome', 
    'sobrenome', 
    'sexo', 
    'email', 
    'senha', 
    'niveldeacesso', 
    'datadecadastro'
);

/* Inserir o primeiro usuário */
insert into users values(
	1,
	'Zaqueu', 
    'Ricardo', 
	1, 
    'zaqueuricheng@teste.com', 
    '123456', 
    1,
    now() /* Pega a data do systema*/
);

/* Inserir o segundo usuário */
insert into users values(
	2,
	'Emaculada', 
    'Fernandes', 
	1, 
    'fernandesema@teste.com', 
    '123456', 
    1,
    now()
);

/* Inserir o terceiro usuário */
insert into users values(
	3,
	'Leticia', 
    'Ricardo', 
	1, 
    'leisrichema@teste.com', 
    '123456', 
    1,
    now()
);

/* */
select *from users;
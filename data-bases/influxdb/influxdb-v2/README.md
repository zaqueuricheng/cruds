https://artifacthub.io/packages/helm/influxdata/influxdb2

helm repo add influxdata https://helm.influxdata.com/
helm upgrade --install my-release influxdata/influxdb2
helm inspect values influxdata/influxdb2 > values.yaml

helm install influxdbv2-review --namespace=data-bases -f values.yaml influxdata/influxdb2 --version 1.1.0

helm search repo bitnami
helm search repo influxdata
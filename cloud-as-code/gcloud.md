# Requeriments
- docker
- chocolatey
- gcloud
- kubernetes/kubectl
- helm
- DBeaver (database tool for developers and database administrators)
    - https://github.com/dbeaver/dbeaver/wiki/Application-Window-Overview

# Login on gcp account
- gcloud auth login
- gcloud logging views --help
- gcloud auth revoke --help

# Set the project
- gcloud projects list
- gcloud config set project <PROJECT_ID>

# List virtual machine and clusters
## List virtual machines
- gcloud compute --help # Create and manipulate compute engine resources
- gcloud compute instances list

## List clusters 
- gcloud container --help # Deploy and manage clusters of machines for running containers
- gcloud container clusters list

# List components and update
- gcloud components list
- gcloud components update

# Create  GKE Cluster 
## Create cluster with linux command line
- export CLUSTER_NAME="your-cluster-name" // democlustercli
- export CLUSTER_ZONE="zone" // us-central1-a | us-east1
- export CLUSTER_REGION="region"

- echo $CLUSTER_NAME
## Create cluster with windows command line
$CLUSTER_NAME="democlustercli"
$CLUSTER_ZONE="us-central1-a"
$PROJECT_ID="velerodemo" # gcloud projects list

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type n1-highcpu-2
- machine-types: https://cloud.google.com/compute/docs/machine-types
    - g1-small
    - n1-standard-2
    - n2-standard-2
    - n1-highcpu-2
    - n1-highmem-2

# Connect to cluster
- gcloud container clusters get-credentials $CLUSTER_NAME --zone $CLUSTER_ZONE --project $PROJECT_ID
- kubectl get pods --all-namespaces

# Insert insert data-bases
- kubectl create ns data-bases
- helm repo list
- helm repo update
- helm repo add bitnami https://charts.bitnami.com/bitnami
- helm search repo bitnami
- helm inspect values bitnami/mysql > k8s-helm-mysqldb-review.yaml

- helm install mysqldb-review-dev --namespace=data-bases -f kubernetes/k8s-helm-mysqldb-review.yaml bitnami/mysql --version 8.5.5
- helm upgrade mysqldb-review-dev --namespace=data-bases -f kubernetes/k8s-helm-mysqldb-review.yaml bitnami/mysql --version 8.5.5
# Connect to your data-bases
- DBeaver

# Troubleshooting (É uma forma de analisar/resolver problemas (qualquer que seja))
- Logs
    - kubectl get deployment/velero-review-backup -n velero
    - kubectl get secret/velero-review-backup -n velero
    - kubectl get pods -n velero
    - kubectl logs YOUR_POD_NAME -n velero
    - kubectl logs YOUR_POD_NAME -n velero | Select-String -Pattern 'warning'
    - kubectl logs YOUR_POD_NAME -n velero | Select-String -Pattern 'error'

- kubectl -n velero delete volumesnapshotlocations --all
- velero backup-location get -o yaml
https://medium.com/@ct.ics2009/install-velero-with-helm-in-gcp-15b7a0186ae

# Emuladores de terminal 
- https://www.treinaweb.com.br/blog/alternativas-para-substituir-o-prompt-de-comando-do-windows/
- iTerm2
- ConEmu
- Hyper
- Terminator
- cmder
- console2
# Requirements
- 1 - gcloud-cli
    - Install gcloud command on Ubuntu
        - sudo snap install google-cloud-sdk --classic
    - Install gcloud command on Windows

- 2 - kubectl-cli

- 3 - velero-cli
```
    - choco install velero
    - choco upgrade velero
    - choco uninstall velero
``` 

# Install gcloud command on windows
# Login in gcp account
- gcloud auth login
- gcloud logging views --help
- gcloud auth revoke --help
# Set the project
- gcloud projects list
- gcloud config set project <PROJECT_ID>

# List virtual machine and clusters
- gcloud compute --help # Create and manipulate compute engine resources
- gcloud compute instances list

- gcloud container --help # Deploy and manage clusters of machines for running containers
- gcloud container clusters list

# List components and update
- gcloud components list
- gcloud components update

# Create  GKE Cluster 
# Create my cluster with linux command line
- export CLUSTER_NAME="your-cluster-name" // democlustercli
- export CLUSTER_ZONE="zone" // us-central1-a | us-east1
- export CLUSTER_REGION="region"

- echo $CLUSTER_NAME
# Create my cluster with windows command line
$CLUSTER_NAME="democlustercli"
$CLUSTER_ZONE="us-central1-a"
$PROJECT_ID="velerodemo" # gcloud projects list

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type n2-standard-2
- machine-types: https://cloud.google.com/compute/docs/machine-types
    - g1-small
    - n1-standard-2
    - n2-standard-2
    - n1-highcpu-2
    - n1-highmem-2

# Connect to cluster
- gcloud container clusters get-credentials $CLUSTER_NAME --zone $CLUSTER_ZONE --project $PROJECT_ID
- kubectl get pods --all-namespaces

# Insert some chart to test backup of the pvc and others kubernestes resources
- Craate namespaces and add chart container
    - kubectl create ns data-bases
    - helm repo list
    - helm repo update
    - helm repo add stable https://charts.helm.sh/stable 
    - helm install mongodb-review --namespace=data-bases -f velero/k8s-helm-mongodb-review.yaml bitnami/mongodb --version 10.3.3

# Configure and create variables to install velero
- Create IAM for GCP
    - $PROJECT_ID=gcloud config get-value project # Config GCP project and get ID
    - gcloud iam service-accounts create velero # Create service account for velero
    - gcloud iam service-accounts list # List all service account
    - $SERVICE_ACCOUNT_EMAIL=gcloud iam service-accounts list --filter velero@clusters-308916.iam.gserviceaccount.com # Get some service account

- Create Bucket for Velero Backup
    - $BUCKET="backupvelero001"
    - gsutil mb gs://$BUCKET/ # Return: Creating gs://backupvelero001/...
    - You can confirm if exist in your console GCP - Cloud Storage menu

- Create Role and mapping permissions on the bucket
        - gcloud iam roles --help
        - gcloud iam roles list
        - gcloud iam roles list --project=$PROJECT_ID

    - You must have permission to create Roles in GCP
        - gcloud iam roles create veleros.server --project=$PROJECT_ID --permissions=resourcemanager.projects.get

        - PERMISSIONS TYPES: https://cloud.google.com/compute/docs/access/iam

        - NOTA: O comando a baixo consegue criar os regras de IAM porém só com uma permissão apois a virgula se colocar a outra da erro
        - gcloud iam roles create veleros.server --project=$PROJECT_ID --permissions=compute.disks.get,compute.disks.create

        - criando um arquivo para ver se resolve #https://cloud.google.com/iam/docs/creating-custom-roles
            - NOTA: Resolveu
            - $ROLE_ID="veleroiam01"
            - gcloud iam roles create $ROLE_ID --project=$PROJECT_ID --file=C:\workspaces\neuron\kwt-cloud-utils\velero\gcp\iam-roles.yaml
            
            - gcloud iam roles describe $ROLE_ID --project=$PROJECT_ID
            - gcloud iam roles delete $ROLE_ID --project=$PROJECT_ID

- SA Associate Bucket
    - gsutil iam ch serviceAccount:my-service-account@project.iam.gserviceaccount.com:objectAdmin gs://my-bucket

    - gsutil iam ch serviceAccount:$SERVICE_ACCOUNT_EMAIL:objectAdmin gs://{$BUCKET}
        - gsutil iam ch serviceAccount:velero@clusters-308916.iam.gserviceaccount.com:objectAdmin gs://$BUCKET

- Create Key of SA (Nota que o comando abaixo criara o arquivo no diretorio onde for executado o comando)
    - gcloud iam service-accounts keys create credentials-velero --iam-account $SERVICE_ACCOUNT_EMAIL
    - See key on project/service account/keys menu GCP console


- Create NS and Secret for Velero
    - kubectl create ns velero
    - kubectl create secret generic cloud-credentials -n velero --from-file cloud=credentials-velero
        - kubectl create secret generic <your-secrete-is-in-chart> -n velero --from-env-file .\velero\credentials-velero
        - kubectl create secret generic cloud-credentials -n velero --from-env-file cloud=credentials-velero
    - kubectl get secret -n velero
    - kubectl describe secret cloud-credentials -n velero

- Configure in the Values.yaml the values ​​of the secret and the bucket created
    - helm repo add vmware-tanzu https://vmware-tanzu.github.io/helm-charts
    - helm inspect values vmware-tanzu/velero > values.yaml # Esse comando "busca" o values.yaml no ropo "INTERESSANTE!"

- Editar Values Velero

- Instalar 
    - helm install velero-review-backup --namespace=velero -f values.yaml vmware-tanzu/velero --version 2.16.0

- Logs after installation
```
- Logs
    - kubectl get deployment/velero-review-backup -n velero
    - kubectl get secret/velero-review-backup -n velero
    - kubectl get pods -n velero
    - kubectl logs YOUR_POD_NAME -n velero
    - kubectl logs YOUR_POD_NAME -n velero | Select-String -Pattern 'warning'
    - kubectl logs YOUR_POD_NAME -n velero | Select-String -Pattern 'error'
```

- kubectl -n velero delete volumesnapshotlocations --all
- velero backup-location get -o yaml


# ############################################### PROCEDIMENTOS QUE FUNCIONOU!!!
- https://github.com/vmware-tanzu/velero-plugin-for-gcp

- $BUCKET="backupvelero001"
- gsutil mb gs://$BUCKET/ # Return: Creating gs://backupvelero001/...
- You can confirm if exist in your console GCP - Cloud Storage menu

- gcloud config list

- $PROJECT_ID=(gcloud config get-value project)
- gcloud iam service-accounts create velero --display-name "Velero service account"
- gcloud iam service-accounts list
- $SERVICE_ACCOUNT_EMAIL=(gcloud iam service-accounts list --filter="displayName:Velero service account" --format 'value(email)')

- $ROLE_ID="velerodemo.server"
- gcloud iam roles create $ROLE_ID --project=$PROJECT_ID --file=C:\workspaces\neuron\kwt-cloud-utils\velero\gcp\iam-roles.yaml

- gcloud projects add-iam-policy-binding $PROJECT_ID --member serviceAccount:$SERVICE_ACCOUNT_EMAIL --role projects/$PROJECT_ID/roles/$ROLE_ID

- gsutil iam ch serviceAccount:$SERVICE_ACCOUNT_EMAIL:objectAdmin gs://$BUCKET
- gsutil iam ch serviceAccount:velero@velerodemo.iam.gserviceaccount.com:objectAdmin gs://$BUCKET # ch atualiza gradualmente as policas

- gcloud iam service-accounts keys create credentials-velero --iam-account $SERVICE_ACCOUNT_EMAIL
- See key on project/service account/keys menu GCP console

# ############################################### VELERO CLI
- velero --help
- velero install --help
- velero install --provider gcp --plugins velero/velero-plugin-for-gcp:v1.2.0 --bucket $BUCKET --secret-file ./credentials-velero
- NOTA: Fez a instalação, porém ficou pendente. Quando fiz um describe no pod, identificou cpu insuficiente o cluster usa(machine type: g1-small, disk: 100 GB, memoria: 1.7 GB, cpu: 1/940mCPU), fiz um "k8s top nodes" possivel solução é criar um outro cluster com uma máquina melhor maior cpu e memoria.
- ***RESOLVEU, aumentando o processador!!!

- https://github.com/vmware-tanzu/helm-charts/blob/main/charts/velero/README.md

PS C:\> kubectl get secret -n velero
NAME                        TYPE                                  DATA   AGE
cloud-credentials           Opaque                                1      14m
default-token-lq4dv         kubernetes.io/service-account-token   3      15m
velero-restic-credentials   Opaque                                1      14m
velero-token-wmhdz          kubernetes.io/service-account-token   3      15m

- kubectl get secret cloud-credentials -n velero -o yaml
- Copia o campo data e cola no site: https://www.base64decode.org/

- Test backups
```
    - velero backup --help
    - velero backup get
    - velero backup create ns-databases-backup-01 --include-namespaces data-bases
    - velero backup describe YOR_BACKUP
    - velero backup-location get -o yaml
```

# ############################################### CHART/VALUES
- https://medium.com/@ct.ics2009/install-velero-with-helm-in-gcp-15b7a0186ae

- Configure in the Values.yaml the values ​​of the secret and the bucket created
    - helm repo add vmware-tanzu https://vmware-tanzu.github.io/helm-charts
    - helm inspect values vmware-tanzu/velero > values.yaml # Esse comando "busca" o values.yaml no ropo "INTERESSANTE!"

- Create velero-credentials file and insert the variables into or create another form like this guide
    - gcloud iam service-accounts keys create credentials-velero --iam-account $SERVICE_ACCOUNT_EMAIL
- kubectl create ns velero
- kubectl create secret generic cloud-credentials -n velero --from-file cloud=credentials-velero
- kubectl get secret -n velero
- kubectl describe secret <your-secrete> -n velero
- kubectl get secret <your-secrete> -n velero -o yaml

- Edit the chart/values
- helm install velero-review-backup --namespace=velero -f values.yaml vmware-tanzu/velero --version 2.14.8

- kubectl logs YOUR_POD_NAME -n velero | Select-String -Pattern 'error'

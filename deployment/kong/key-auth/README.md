## AUTENTICAÇÃO/Segurança

# ACL (Access Control List) - Contains rules that grant or deny access to certain digital environments

# TLS (Transport Layer Security) - 

# JWT (Json Web Token) - Is an open standard that defines a compact and self-contained way for securely transmitting information between parties as a JSON object.

## Cluster
- gcloud projects list
- gcloud config set project <PROJECT_ID>

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"

- gcloud container clusters create $CLUSTER_NAME --num-nodes 1 --zone $CLUSTER_ZONE --machine-type n2-standard-2

# READ
- https://docs.konghq.com/hub/kong-inc/key-auth-enc/

# Update User Permissions
- kubectl apply -f role-binding.yaml -n kube-system

# Deploy the Kubernetes Ingress Controller
- kubectl create -f https://bit.ly/k4k8s

# Setup an environment variable to hold the IP address
- export PROXY_IP=$(kubectl get -o jsonpath="{.status.loadBalancer.ingress[0].ip}" service -n kong kong-proxy)

# Testing Connectivity to Kong
- curl -i $PROXY_IP

# Setup a Sample Service
- kubectl apply -f https://bit.ly/k8s-httpbin

# Create an Ingress rule to proxy the httpbin service
- kubectl apply -f Ingress-httpdin.yaml

# Test the Ingress rule
curl -i $PROXY_IP/foo/status/200

# Add authentication to the service
- kubectl apply -f auth-svc.yaml

# Now, associate this plugin with the previous Ingress rule we created using the konghq.com/plugins annotation
- kubectl apply -f Ingress-httpdin.yaml

## Test
# Any request matching the proxying rules defined in the demo ingress will now require a valid API key
- curl -i $PROXY_IP/foo/status/200

# Provision a Consumer
- kubectl apply -f consumer.yaml

# Now, let’s provision an API-key associated with this consumer so that we can pass the authentication imposed by Kong
- kubectl create secret generic harry-apikey --from-literal=kongCredType=key-auth --from-literal=key=my-sooper-secret-key

# Associate this API-key with the consumer we created previously
- kubectl apply -f consumer.yaml

# 
- kubectl get secret harry-apikey -n default -o yaml
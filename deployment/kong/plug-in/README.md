- https://docs.konghq.com/kubernetes-ingress-controller/1.1.x/guides/configure-acl-plugin/?_ga=2.227574109.194714481.1620061771-177187029.1620061771#use-the-credential

- https://learnk8s.io/kubernetes-ingress-api-gateway#:~:text=Kong%20is%20an%20API%20gateway,packaged%20as%20a%20Kubernetes%20Ingress.

## AUTENTICAÇÃO/Segurança

# ACL (Access Control List) - Contains rules that grant or deny access to certain digital environments

# TLS (Transport Layer Security) - 

# JWT (Json Web Token) - Is an open standard that defines a compact and self-contained way for securely transmitting information between parties as a JSON object.

## Using Power Shell line command
- curl 35.223.220.213/bar

## Using bash line command
- curl -i 35.223.220.213

# Deployment and Service
- kubectl apply -f https://bit.ly/k8s-httpbin -n kong

# Test in localhost
- kubectl get pods,deploy,svc,ing,secret,pvc -n kong
- kubectl port-forward httpbin-5b55598456-f7qbk 80:80 -n kong

# Create an ingress for this service
- kubectl apply -f get-ingress.yaml -n kong
- kubectl apply -f post-ingress.yaml -n kong

# Test the Ingress rules
- curl -i 35.223.220.213/get
- curl -i --data "foo=bar" -X POST 35.223.220.213/post

# Add JWT authentication plugin to the service
kubectl apply -f auth.yaml -n kong

# Associate the plugin to the Ingress rules we created earlier
- https://docs.konghq.com/hub/kong-inc/jwt/

- Edit the line
    annotations:
        konghq.com/plugins: app-jwt

- kubectl apply -f get-ingress.yaml -n kong
- kubectl apply -f post-ingress.yaml -n kong

# Test again and the both request is not authorized
- curl -i 35.223.220.213/get
- curl -i --data "foo=bar" -X POST 35.223.220.213/post

# Provision Consumers
- kubectl apply -f consumers-plain.yaml -n kong
- kubectl apply -f consumers-admin.yaml -n kong

# Create secret
- kubectl create secret generic app-admin-jwt --from-literal=kongCredType=jwt --from-literal=key="admin-issuer" --from-literal=algorithm=RS256 --from-literal=rsa_public_key="-----BEGIN PUBLIC KEY-----MIIBIjA....-----END PUBLIC KEY-----" -n kong

- kubectl create secret generic app-user-jwt --from-literal=kongCredType=jwt --from-literal=key="user-issuer" --from-literal=algorithm=RS256 --from-literal=rsa_public_key="-----BEGIN PUBLIC KEY-----qwerlkjqer....-----END PUBLIC KEY-----" -n kong

# Update the consumers to use the credentials that we created
- kubectl apply -f consumers-plain.yaml -n kong
- kubectl apply -f consumers-admin.yaml -n kong

# Test
- kubectl get pods,deploy,svc,ing,secret,pvc,kongconsumer -n kong

- kubectl get secret app-admin-jwt -n kong -o yaml

- curl -i 35.223.220.213/get
- curl -i -H "Authorization: Bearer ${USER_JWT}" 35.223.220.213/get

- curl -i -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRYTmxjaTFwYzNOMVpYST0ifQ.eyJuYW1lIjoicGxhaW4tdXNlciJ9.C8n0fX6772IqNjPPGH6a8uqRzqWcPhYN-NTvLzjJP24" 35.223.220.213/get
const express = require('express');
const app = express();

app.get('/bar', (req, res) => {
  // console.log('Hello world received a request.');

  // const target = process.env.TARGET || 'World';
  // res.send(`Hello ${target}!`);
  res.send("Hello from the bar microservice");
});

const port = process.env.PORT || 80;
app.listen(port, () => {
  console.log('Hello world listening on port', port);
});
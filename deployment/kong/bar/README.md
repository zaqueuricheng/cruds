https://www.youtube.com/watch?v=DwlIn9zOcfc


https://stackoverflow.com/questions/35266769/how-to-list-the-published-container-images-in-the-google-container-registry-usin


## Steps
https://cloud.google.com/kubernetes-engine/docs/quickstarts/deploying-a-language-specific-app?hl=pt-br#standard
# 1 - package.json

# 2 - index.js

# 3 - Dockerfile

# 4 - .dockerignore

# 5 - Cria uma imagem

- gcloud projects list
- gcloud config set project ID
- gcloud config get-value project

- $CLUSTER_NAME="democlustercli"
- $CLUSTER_ZONE="us-central1-a"
- gcloud container clusters resize $CLUSTER_NAME --num-nodes=1 --zone $CLUSTER_ZONE

- gcloud builds submit --tag gcr.io/project-id/helloworld-gke .
- gcloud builds submit --tag gcr.io/monitoramento-valid/helloworld-nodejs-gke .

# 6 - Implantar o aplicativo
- objeto de implantação
    - deployment.yaml
    - kubectl create ns my-applications
    - kubectl apply -f deployment.yaml -n my-applications
    - kubectl get deployments

- objeto service
    - service.yaml
    - kubectl apply -f service.yaml -n my-applications
    - kubectl get services -n my-applications
    - curl 35.238.229.59 

# Realizar limpeza para evitar cobranças
- Excluir o projeto a partir do console do google
- Excluir o cluster e o contêiner
    -  gcloud container clusters delete $CLUSTER_NAME --region $CLUSTER_REGION
    -  gcloud container images delete $IMAGEM


## New
# List images
- gcloud container images list

# Delete images
- gcloud container images delete gcr.io/monitoramento-valid/bar
- gcloud container images delete gcr.io/monitoramento-valid/foo

- gcloud builds submit --tag gcr.io/monitoramento-valid/bar:0.0.2 . // Copy the image...

## Deployment
- kubectl apply -f bar.yaml -n my-applications

## Service
- kubectl apply -f service.yaml -n my-applications

## Teste
- kubectl port-forward bar-deployment-6f5d777956-mspjv 80:8080

## After install kong
- kubectl apply -f bar-ingress.yaml -n my-applications

## New
## Create image

## Create deploy and service
kubectl apply -f foo.yaml -n kong

# Test if the svc is normal

# Install kong
- helm install kong/kong --generate-name --set ingressController.installCRDs=false -n kong

# Create Ingress
- kubectl apply -f bar-ingress.yaml -n kong